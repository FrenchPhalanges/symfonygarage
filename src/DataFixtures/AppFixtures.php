<?php

namespace App\DataFixtures;

use App\Entity\Garage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");


        for ($i = 0; $i < 10; $i++)
        {
            $garage = new Garage();
            $garage -> setMarque($faker->name())
                    -> setCouleur($faker->colorName())
                    -> setPrix($faker->randomNumber(5,false))
                    -> setDescription($faker->paragraph(5,true));
            $manager ->persist($garage);        
                    
        }
        $manager->flush();

    }
}
